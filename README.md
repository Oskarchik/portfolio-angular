# FinalProjectUpgradeApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Description Project

TODO: Descripción de nuestra aplicación de Angular -> En qué consiste? Para qué sirve?

## Run Project

TODO: Pasos de arranque del proyuecto -> npm i / ng serve / localhost:4200

## Run Test

TODO: Introducir Jest + Spectator -> Opcional

## Prod Project

TODO: Generar proyecto para Dist -> Subir a Prod / Netlify

## Scully

TODO: Como generar Scully Prod -> ng build / npm run scully / npm run scully:serve -> http://localhost:1668

## Libs

TODO: Swiper + Lottie -> Explicar su uso (con un componente)

## Modules

TODO: Extra -> explicar lazy-loading de modules

## CSS

TODO: Extra -> Arquitectura de css
